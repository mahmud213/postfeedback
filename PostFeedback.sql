USE [master]
GO
/****** Object:  Database [PostFeedback]    Script Date: 22-Oct-20 1:17:27 AM ******/
CREATE DATABASE [PostFeedback]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PostFeedback', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PostFeedback.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PostFeedback_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PostFeedback_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [PostFeedback] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PostFeedback].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PostFeedback] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PostFeedback] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PostFeedback] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PostFeedback] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PostFeedback] SET ARITHABORT OFF 
GO
ALTER DATABASE [PostFeedback] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PostFeedback] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PostFeedback] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PostFeedback] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PostFeedback] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PostFeedback] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PostFeedback] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PostFeedback] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PostFeedback] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PostFeedback] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PostFeedback] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PostFeedback] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PostFeedback] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PostFeedback] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PostFeedback] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PostFeedback] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PostFeedback] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PostFeedback] SET RECOVERY FULL 
GO
ALTER DATABASE [PostFeedback] SET  MULTI_USER 
GO
ALTER DATABASE [PostFeedback] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PostFeedback] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PostFeedback] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PostFeedback] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PostFeedback] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PostFeedback', N'ON'
GO
ALTER DATABASE [PostFeedback] SET QUERY_STORE = OFF
GO
USE [PostFeedback]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [PostFeedback]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 22-Oct-20 1:17:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[PostId] [int] IDENTITY(1,1) NOT NULL,
	[PostContent] [nvarchar](1000) NULL,
	[PostDate] [datetime] NULL,
	[PostBy] [varchar](100) NULL,
 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PostComment]    Script Date: 22-Oct-20 1:17:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostComment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[PostId] [int] NOT NULL,
	[CommentMessage] [varchar](500) NULL,
	[CommentBy] [varchar](100) NOT NULL,
	[CommentDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PostComment] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserFeedback]    Script Date: 22-Oct-20 1:17:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserFeedback](
	[FeedbackId] [int] IDENTITY(1,1) NOT NULL,
	[CommentId] [int] NOT NULL,
	[UserId] [varchar](100) NOT NULL,
	[LikeDislike] [varchar](10) NULL,
 CONSTRAINT [PK_CommentFeedback] PRIMARY KEY CLUSTERED 
(
	[FeedbackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Post] ON 
GO
INSERT [dbo].[Post] ([PostId], [PostContent], [PostDate], [PostBy]) VALUES (1, N'Post1', CAST(N'2020-11-21T00:00:00.000' AS DateTime), N'Admin')
GO
INSERT [dbo].[Post] ([PostId], [PostContent], [PostDate], [PostBy]) VALUES (2, N'Post2', CAST(N'2020-11-21T00:00:00.000' AS DateTime), N'Admin')
GO
INSERT [dbo].[Post] ([PostId], [PostContent], [PostDate], [PostBy]) VALUES (3, N'Post3', CAST(N'2020-11-21T00:00:00.000' AS DateTime), N'Admin')
GO
SET IDENTITY_INSERT [dbo].[Post] OFF
GO
SET IDENTITY_INSERT [dbo].[PostComment] ON 
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (1, 1, N'Comment 1', N'User1', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (2, 1, N'Comment 2', N'User2', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (3, 1, N'Comment 3', N'User3', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (4, 2, N'Comment 4', N'User1', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (5, 2, N'Comment 5', N'User2', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (6, 2, N'Comment 6', N'User3', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (7, 2, N'Comment 7', N'User3', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[PostComment] ([CommentId], [PostId], [CommentMessage], [CommentBy], [CommentDate]) VALUES (8, 3, N'Comment 8', N'User1', CAST(N'2020-11-21T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[PostComment] OFF
GO
SET IDENTITY_INSERT [dbo].[UserFeedback] ON 
GO
INSERT [dbo].[UserFeedback] ([FeedbackId], [CommentId], [UserId], [LikeDislike]) VALUES (1, 1, N'User1', N'LIKE')
GO
INSERT [dbo].[UserFeedback] ([FeedbackId], [CommentId], [UserId], [LikeDislike]) VALUES (2, 1, N'User2', N'LIKE')
GO
INSERT [dbo].[UserFeedback] ([FeedbackId], [CommentId], [UserId], [LikeDislike]) VALUES (3, 1, N'User3', N'DISLIKE')
GO
INSERT [dbo].[UserFeedback] ([FeedbackId], [CommentId], [UserId], [LikeDislike]) VALUES (4, 2, N'User1', N'LIKE')
GO
INSERT [dbo].[UserFeedback] ([FeedbackId], [CommentId], [UserId], [LikeDislike]) VALUES (5, 2, N'User4', N'LIKE')
GO
INSERT [dbo].[UserFeedback] ([FeedbackId], [CommentId], [UserId], [LikeDislike]) VALUES (6, 2, N'User3', N'LIKE')
GO
SET IDENTITY_INSERT [dbo].[UserFeedback] OFF
GO
USE [master]
GO
ALTER DATABASE [PostFeedback] SET  READ_WRITE 
GO
