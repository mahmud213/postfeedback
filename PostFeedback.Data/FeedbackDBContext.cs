﻿using PostFeedback.Data.Modles;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace PostFeedback.Data
{
    public class FeedbackDBContext : DbContext
    {
        public FeedbackDBContext(DbContextOptions<FeedbackDBContext> options)
           : base(options)
        {
        }

        public DbSet<Post> Post { get; set; }
        public DbSet<PostComment> PostComment { get; set; }
        public DbSet<UserFeedback> UserFeedback { get; set; }
    }
}
