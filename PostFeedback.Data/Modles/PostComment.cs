﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PostFeedback.Data.Modles
{
    public class PostComment
    {
        [Key]
        public int CommentId { get; set; }
        public int PostId { get; set; }
        public string CommentMessage { get; set; }
        public DateTime CommentDate { get; set; }
        public string CommentBy { get; set; }


    }
}
