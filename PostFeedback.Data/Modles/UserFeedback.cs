﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PostFeedback.Data.Modles
{
    public class UserFeedback
    {
        [Key]
        public int FeedbackId { get; set; }
        public int CommentId { get; set; }
        public string UserId { get; set; }
        public string LikeDislike { get; set; }

    }
}
