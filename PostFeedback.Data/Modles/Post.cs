﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PostFeedback.Data.Modles
{
    public class Post
    {
        [Key]
        public int PostId { get; set; }
        public string PostContent { get; set; }
        public DateTime PostDate { get; set; }
        public string PostBy { get; set; }

    }
}
