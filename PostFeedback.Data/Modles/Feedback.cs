﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostFeedback.Data.Modles
{
    public class Feedback
    {
        public int FeedbackId { get; set; }
        public string Message { get; set; }
        public DateTime FeedbackDate { get; set; }
        public string FeedbackBy { get; set; }
        public int PostID { get; set; }
        public int Like { get; set; }
        public int Dislike { get; set; }
        public string Post { get; set; }
    }
}
