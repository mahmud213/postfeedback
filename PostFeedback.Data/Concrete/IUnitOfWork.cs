﻿using PostFeedback.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace PostFeedback.Data.Concrete
{
    public interface IUnitOfWork
    {
        PostRepository PostRepo { get; }
        PostCommentRepository PostCommentRepo { get; }
        UserFeedbackRepository UserFeedbackRepo { get; }

    }
}
