﻿using PostFeedback.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace PostFeedback.Data.Concrete
{
    public class UnitOfWorks : IUnitOfWork
    {
        private FeedbackDBContext _dbContext;
        private PostRepository _postRepo;
        private PostCommentRepository _postCommentRepo;
        private UserFeedbackRepository _userFeedbackRepo;

        public FeedbackDBContext FbDBContext
        {
            get
            {
                return this._dbContext;
            }

        }
        public UnitOfWorks(FeedbackDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public PostRepository PostRepo
        {
            get
            {
                if (_postRepo == null)
                {
                    _postRepo = new PostRepository(_dbContext);
                }
                return _postRepo;
            }

        }

        public PostCommentRepository PostCommentRepo
        {
            get
            {
                if (_postCommentRepo == null)
                {
                    _postCommentRepo = new PostCommentRepository(_dbContext);
                }
                return _postCommentRepo;
            }

        }

        public UserFeedbackRepository UserFeedbackRepo
        {
            get
            {
                if (_userFeedbackRepo == null)
                {
                    _userFeedbackRepo = new UserFeedbackRepository(_dbContext);
                }
                return _userFeedbackRepo;
            }

        }
    }
}
