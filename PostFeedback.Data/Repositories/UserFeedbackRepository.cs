﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using PostFeedback.Data.Modles;

namespace PostFeedback.Data.Repositories
{
    public class UserFeedbackRepository
    {
        private FeedbackDBContext _dBContext;

        public UserFeedbackRepository(FeedbackDBContext dbContext)
        {
            this._dBContext = dbContext;
        }

        public IEnumerable<UserFeedback> GetAllUserFeedback()
        {
            return this._dBContext.UserFeedback;
        }

        public IEnumerable<Feedback> GetFeedback()
        {
            return (from c in _dBContext.PostComment.AsNoTracking()
                    join p in _dBContext.Post.AsNoTracking() on c.PostId equals p.PostId
                    select new Feedback
                    {
                        Message = c.CommentMessage,
                        FeedbackDate = c.CommentDate,
                        FeedbackBy = c.CommentBy,
                        Post = p.PostContent,
                        Like = (from f in _dBContext.UserFeedback.AsNoTracking() select f).Where(t => t.LikeDislike == "LIKE" && t.CommentId == c.CommentId).Count(),
                        Dislike = (from f in _dBContext.UserFeedback.AsNoTracking() select f).Where(t => t.LikeDislike == "DISLIKE" && t.CommentId == c.CommentId).Count()
                    });
        }
    }
}
