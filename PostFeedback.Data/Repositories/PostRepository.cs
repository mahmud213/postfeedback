﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using PostFeedback.Data.Modles;

namespace PostFeedback.Data.Repositories
{
    public class PostRepository
    {
        private FeedbackDBContext _dBContext;

        public PostRepository(FeedbackDBContext dbContext)
        {
            this._dBContext = dbContext;
        }

        public IEnumerable<Post> GetAllPost()
        {
            return this._dBContext.Post;
        }
        
        public Post GetById(int? id)
        {
            return this._dBContext.Post.Find(id);
        }
    }
}
