﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using PostFeedback.Data.Modles;

namespace PostFeedback.Data.Repositories
{
    public class PostCommentRepository
    {
        private FeedbackDBContext _dBContext;


        public PostCommentRepository(FeedbackDBContext dbContext)
        {
            this._dBContext = dbContext;
        }
        public IEnumerable<PostComment> GetAll()
        {
            return this._dBContext.PostComment;
        }
        public PostComment GetById(int? id)
        {
            return this._dBContext.PostComment.Find(id);
        }
    }
}
